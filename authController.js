import User from './models/User.js';
// import Role from './models/Role.js';
import Note from './models/Note.js';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import {validationResult} from 'express-validator';

//AUTH
const generateAccesToken = (id, roles) => {
  const payload = {
    id,
    roles
  }
  return jwt.sign(payload, 'secret', {expiresIn: '24h'});
}

class authController {
  async registration(req, res) {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({message:'Registration error ', errors});
      }
      const {username, password, createdDate} = req.body //user1, 123, user2 1234
      const candidate = await User.findOne({username});
      if (candidate) {
        return res.status(400).json({message:'User already exists '});
      }
      const hashPassword = bcrypt.hashSync(password, 7);
      // const userRole = await Role.findOne({value: 'USER'})
      const user = new User({username, password: hashPassword, createdDate: new Date()});//roles: [userRole.value]
      await user.save()
      return res.json({message:'Success'});
    } catch(e) {
      res.status(500).json({message:'Server error'})
    }
  }

  async login(req, res) {
    try {
      const {username, password} = req.body
      const user = await User.findOne({username});
      if (!user) {
        return res.status(400).json({message: `The user ${username} can't be found`});
      }
      const validPassword = bcrypt.compareSync(password, user.password)
      if(!validPassword) {
        return res.status(400).json({message: 'Incorrect password'});
      }
      const jwt_token = generateAccesToken(user._id, user.roles)
      return res.json({message:'Success', jwt_token});
    } catch(e) {
      res.status(500).json({message:'Server error'})
    }
  }
  
  //USER
  async getUserProfile(req, res) {
    try {
      // const userRole = new Role()
      // const adminRole = new Role({value: 'ADMIN'})
      // await userRole.save()
      // await adminRole.save()
      const {authorization} = req.headers
      if (!authorization) { 
        return res.status(400).json({message: 'Token was not found'})
      }
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const user = await User.findOne({_id: decoded.id}).exec()
      return res.json({user: user})
    } catch(e) {
      res.status(500).json({message:'Server error'}) 
    }
  }

  async deleteUserProfile(req, res) { 
    try {
      const {authorization} = req.headers
      if (!authorization) { 
        return res.status(400).json({message: 'Not found'})
      }
      const token = authorization.split(' ')[1]
      const verified = jwt.verify(token, 'secret')
      await User.findByIdAndDelete({_id: verified.id})
      return res.json({message:'Success'})
    } catch(e) {
      res.status(500).json({message:'Server error'}) 
    }
  }

  async changePassword(req, res) {
    try {
      const {authorization} = req.headers
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const newPassword = req.body.newPassword
      const hashNewPassword = bcrypt.hashSync(newPassword, 7);
      const result = await User.findByIdAndUpdate({_id: decoded.id}, {$set: {password: hashNewPassword}})
        console.log(result)
        return res.json({message:'Success'})
    } catch(e) {
      console.log(e)
      res.status(500).json({message:'Server error'}) 
    }
  }
  
  //NOTES
  async addNote(req, res) {  
    try {
      const {authorization} = req.headers
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const userId = decoded.id
      const text = req.body.text
      const note = new Note({userId, text, createdDate: new Date()});
      note.user = userId;
      await note.save(err => {
        if (err) {
          console.log(err)
        }
      })
      return res.json({message:'Success'});
    } catch(e) {
      res.status(500).json({message:'Server error'})
    }
  }

  async getNotes(req, res) {
    try {
      const {authorization} = req.headers
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const userId = decoded.id
      const notes = await Note.find({userId}).exec()
      return res.json({notes: notes});
    } catch(e) {
      res.status(500).json({message:'Server error'})
    }
  }

  async getOne(req, res) {
    try{
      const {id} = req.params
        if (!id) {
          res.status(400).json({message: 'Not found'})
        }
      const note = await Note.findById(id)
      return res.json(note)
    } catch(e) {
      res.status(500).json({message:'Server error'})
    }
  }

  async update(req, res) {
    try{
      const note = req.body
      const {id} = req.params
      const updatedNote = await Note.findByIdAndUpdate(id, note, {new: true})
      if (!updatedNote) {
        return res.status(400).json({message: 'Not found'})
      }
      return res.json({message:'Success'})
    } catch(e) {
      res.status(500).json({message:'Server error'})
    }
  }

  async delete(req, res) {
    try{
      const {id} = req.params
      if (!id) {
        res.status(400).json({message: 'Not found'})
      }
      const note = await Note.findByIdAndDelete(id)
      return res.json({message:'Success'})
    } catch(e) {
      res.status(500).json({message:'Server error'})
    }
  }

}

const controller = new authController();
export default controller
