// import fs from 'fs'
import express from 'express'
import mongoose from 'mongoose'
import router from './authRouter.js'
const PORT = 8080
const app = express()

app.use(express.json());
app.use(router)

const start = async() => {
  try {
    await mongoose.connect('mongodb+srv://Kyrylo:ve518dl3@cluster0.vskcw.mongodb.net/Auth_api?retryWrites=true&w=majority')
    app.listen(PORT, () => console.log('running...'));
  } catch (e) {
    console.log(e)
  }
}

start()

