import mongoose from 'mongoose';
// import User from './User';
const { Schema, model } = mongoose;

const Note = new Schema({
  userId: {type : String, ref: 'User', require : true},
  // userId: {type: String, ref: 'User', required: true},
  // completed: {type: Boolean, required: true},
  text: {type: String, required: true},
  createdDate: {type: Date, required: true}
})

export default model('Note', Note);
