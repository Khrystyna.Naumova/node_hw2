import { Router } from 'express'
import controller from './authController.js'
import {check} from 'express-validator'
const router = new Router()

//AUTH
router.post('/api/auth/register', [
  check('username', 'Username cannot be empty').notEmpty(),
  check('password', 'Password should be more 2 and less than 7').isLength({ min:2, max:10 })
],
controller.registration);
router.post('/api/auth/login', controller.login);

//USER
router.get('/api/users/me', controller.getUserProfile);
router.delete('/api/users/me', controller.deleteUserProfile);
router.patch('/api/users/me', controller.changePassword)

//NOTES
router.post('/api/notes', controller.addNote);
router.get('/api/notes', controller.getNotes);
router.get('/api/notes/:id', controller.getOne);
router.put('/api/notes/:id', controller.update);
router.delete('/api/notes/:id', controller.delete);

export default router